CC = g++ -Wall -pedantic -ansi -g
LIBS = -lmysqlclient
CFLAGS = -I/usr/include/mysql

all: po2mysql check_consistency

po2mysql: po2mysql.o
	$(CC) -o po2mysql po2mysql.o $(LIBS)

po2mysql.o: po2mysql.c po2mysql.h
	$(CC) -c po2mysql.c $(CFLAGS) 

check_consistency: check_consistency.o
	$(CC) -o check_consistency check_consistency.o $(LIBS)

check_consistency.o: check_consistency.c check_consistency.h
	$(CC) -c check_consistency.c $(CFLAGS)

install: po2mysql check_consistency
	cp po2mysql check_consistency /usr/local/bin
