/*  Copyright (c) 2002 Filipe Maia <fmaia@gmx.net>. All rights reserved.   *
 *                                                                         *
 * Permission is hereby granted, free of charge, to any person obtaining a *
 * copy of this software and associated documentation files (the           *
 * "Software"), to deal in the Software without restriction, including     *
 * without limitation the rights to use, copy, modify, merge, publish,     *
 * distribute, and/or sell copies of the Software, and to permit persons   *
 * to whom the Software is furnished to do so, provided that the above     *
 * copyright notice(s) and this permission notice appear in all copies of  *
 * the Software and that both the above copyright notice(s) and this       *
 * permission notice appear in supporting documentation.                   *
 *                                                                         *              
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS * 
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF              *
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT   *
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR        *
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL *
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING  *
 * FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,    *
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION    *
 * WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.                           */

#ifdef __cplusplus
extern "C" {
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <mysql.h>
#include <getopt.h>
#include <termios.h>
#include <time.h>
  char *strptime(const char *s, const char  *format,  struct
		 tm *tm);
}
#endif /* __cplusplus */
#include <vector.h>

#define MAIN_TABLE "main"
#define ORIGINAL_SUF "_orig"
#define DEFAULT_DB "gnome_translations"
#define DEFAULT_HOST "localhost"
#define VERSION "0.1"
#define ID "check_consistency"

int main(int argc,char **argv);
int init(MYSQL ** new_db);
int parse_options(int argc,char ** argv);
int connect(MYSQL * pointer_to_db);
void usage();
void version();
int close_connection(MYSQL * mysql);
int compare_datetimes(char * datetime1, char * datetime2);
char ** get_package_list(MYSQL * mysql);
int consistency_check(MYSQL* mysql,char ** package_list);
char * make_safe_for_insertion(char * string);
int check_string(MYSQL * mysql,char ** package_list,char * string);
int check_string_id(MYSQL * mysql,char * package_name,char *  string_id);
int print_header(MYSQL * mysql,char * package_name,int string_id);
void print_results(MYSQL * mysql);
