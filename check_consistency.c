/*  Copyright (c) 2002 Filipe Maia <fmaia@gmx.net>. All rights reserved.   *
 *                                                                         *
 * Permission is hereby granted, free of charge, to any person obtaining a *
 * copy of this software and associated documentation files (the           *
 * "Software"), to deal in the Software without restriction, including     *
 * without limitation the rights to use, copy, modify, merge, publish,     *
 * distribute, and/or sell copies of the Software, and to permit persons   *
 * to whom the Software is furnished to do so, provided that the above     *
 * copyright notice(s) and this permission notice appear in all copies of  *
 * the Software and that both the above copyright notice(s) and this       *
 * permission notice appear in supporting documentation.                   *
 *                                                                         *              
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS * 
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF              *
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT   *
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR        *
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL *
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING  *
 * FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,    *
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION    *
 * WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.                           */

#include "check_consistency.h"

int port; 
char * user; 
char use_passwd;
char * host;
char * database;
char * idiom= NULL;
FILE * file;


char ** packages_to_check = NULL;

/* Only piece of code that uses STL*/
vector <char *> different_translations;
vector <char *> different_packages;

/* Really ugly global variables */
int header_string_id;
char * header_package_name;

int main(int argc,char *argv[]){
  int ret;
  char ** package_list;
  MYSQL * mysql;
  ret = init(&mysql);
  if (ret){
    return ret;
  }
  ret = parse_options(argc,argv);
  if (ret){
    return ret;
  }
  ret = connect(mysql);
  if (ret){
    return ret;
  }
  package_list = get_package_list(mysql);
  if (!package_list) {
    return errno;
  }
 
  ret = consistency_check(mysql,package_list);
  if (ret){
    return ret;
  }
 
  ret = close_connection(mysql);
  if (ret){
    return ret;
  }
  return 0;
}

/*
 * Initializes the global variables and the MYSQL structure
 */
int init(MYSQL ** new_db){
  MYSQL * mysql;
  /* We'll print to stdout*/
  file = stdout;
  /* I'm not sure if mysql_init sets errno so i'm setting it manually */
  mysql = mysql_init(0);  
  if (!mysql) {
    errno = 12;
    perror(ID);
    return errno;
  }
  use_passwd = 0;
  port = 0;
  user = NULL;
  database = NULL;
  host = (char *)malloc(sizeof(char) * (strlen(DEFAULT_HOST)+1));
  if (!host) {
    perror(ID);
    return errno;
  }  
  strcpy(host,DEFAULT_HOST);
  database = (char *)malloc(sizeof(char) * (strlen(DEFAULT_DB)+1));
  if (!database) {
    perror(ID);
    return errno;
  }  
  strcpy(database,DEFAULT_DB);
  *new_db = mysql;
  return 0;
}


/*
 * Sets the global variables according to the options passed
 */
int parse_options(int argc,char ** argv){
  char c;
  int i;
  static struct option long_options[] = {
    {"passwd", 0, 0, 'p'},
    {"host", 1, 0, 'H'},
    {"port", 1, 0, 'P'},
    {"user", 1, 0, 'u'},
    {"help", 0, 0, 'h'},
    {"version", 0, 0, 'v'},    
    {"database", 1, 0, 'D'},
    {"idiom",1,0,'i'},
    {0, 0, 0, 0}
  };

  while((c = getopt_long(argc,argv,"vphD:H:P:u:i:",long_options,NULL)) != -1){
    switch(c){
    case 'p':
      use_passwd = 1;
      break;
    case 'H':
      if(optarg && strlen(optarg)){
	free(host);
	host = (char *)malloc(sizeof(char)*strlen(optarg)+1);
	strcpy(host,optarg);
      } else {
	fprintf(stderr,"%s: Passing null pointer as argument in %s:%d\n",ID,__FILE__,__LINE__);
      }
      break;
    case 'i':
      if(optarg && strlen(optarg)){
	idiom = (char *)malloc(sizeof(char)*strlen(optarg)+1);
	strcpy(idiom,optarg);
      } else {
	fprintf(stderr,"%s: Passing null pointer as argument in %s:%d\n",ID,__FILE__,__LINE__);
      }
      break;
    case 'P':
      if(optarg && strlen(optarg)){
	errno = 0;
	port = strtol(optarg,NULL,10);
	if (errno){
	  perror(ID);
	  port = 0;
	}
      } else {
	fprintf(stderr,"%s: Passing null pointer as argument in %s:%d\n",ID,__FILE__,__LINE__);
      }    
      break;
    case 'u':
      if(optarg && strlen(optarg)){
	user = (char *)malloc(sizeof(char)*strlen(optarg)+1);
	strcpy(user,optarg);
      } else {
	fprintf(stderr,"%s: Passing null pointer as argument in %s:%d\n",ID,__FILE__,__LINE__);
      }
      break;
    case 'D':
      if(optarg && strlen(optarg)){
	free(database);
	database = (char *)malloc(sizeof(char)*strlen(optarg)+1);
	strcpy(database,optarg);
      } else {
	fprintf(stderr,"%s: Passing null pointer as argument in %s:%d\n",ID,__FILE__,__LINE__);
      }
      break;
    case 'h':
      usage();
      exit(0);
      break;
    case 'v':
      version();
      exit(0);
      break;
    default:
      fprintf(stderr,"%s: Error parsing options\n",ID);
    }
  }
  if(!idiom){
    fprintf(stderr,"You need to specify a idiom!\n");
    exit(-1);
  }

  /*
   * Assumes the rest of the options are the packages to check
   * and set the global list char ** packages_to_check accordingly
   */
  packages_to_check = (char **)malloc(sizeof(char *)*(argc-optind+1));
  if(!packages_to_check){
    perror(ID);
    return errno;
  }
  packages_to_check[argc-optind] = NULL;
  for (i = optind;i < argc;i++) {
    packages_to_check[i-optind] = (char *)malloc(sizeof(char)*strlen(argv[i])+1);
    if(!packages_to_check[i-optind]){
      perror(ID);
      return errno;
    }
    strcpy(packages_to_check[i-optind],argv[i]);
  }
  return 0;
}


/*
 * Simply prints the a small help
 */
void usage(){
  printf("Usage: %s [OPTIONS] -i idiom [packages]\n",ID);
  printf("\n");
  printf(" -H, --host=HOST\t set the hostname or IP of the mysql server\n"); 
  printf(" -u, --user=USER\t set the username used to connect to the mysql server\n");
  printf(" -P, --port=PORT\t set the port where the mysql server is listening\n"); 
  printf(" -D, --database=DB\t set the name of the database to be used\n");
  printf(" -p, --passwd\t\t use password to connect to the mysql server\n");
  printf(" -i, --idiom\t\t idiom to be checked\n");
  printf(" -h, --help\t\t prints this small help\n");
  printf("\n");
}

/*
 * Prints version and copyright
 */
void version(){
  printf("%s %s\n",ID,VERSION);
  printf("Written by Filipe Maia <fmaia@gmx.net>\n");
  printf("\n");
  printf("Copyright (C) 2002 Filipe Maia\n");
  printf("This is free software; see the source for copying conditions.  There is NO\n");
  printf("warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n");
}


/*
 * Connect to the database according to the values of the global variables 
 * and if succesful points the argument to the created MYSQL * database.
 */
int connect(MYSQL * mysql){
  /*
   * I'm using a static variable for passwd instead of a pointer for security
   * This way i don't have to dinamically allocate memory
   */

  char passwd[100];
  int i;
  char c;
  char * pointer_to_passwd;
  char * buffer;

  struct termios initial_set,new_set;

  memset(passwd,0,100);

  if(use_passwd){
    /* I'm sending this through stderr so it can still be seen when someone redirects the output to a file */
    fprintf(stderr,"Password: ");
    tcgetattr(0,&initial_set);
    new_set= initial_set;
    new_set.c_lflag &= ~ECHO;
    tcsetattr(0,TCSANOW,&new_set);
    i = 0;
    do{
      c = getchar();
      if (c != '\n')
	passwd[i] = c;
      i++;    
    } while( c !='\n' && i<100);
    tcsetattr(0,TCSANOW,&initial_set);
    fprintf(stderr,"\n");
  }
  
  if(!passwd[0]){
    pointer_to_passwd = NULL;
  } else {
    pointer_to_passwd = passwd;
  }

  if (!mysql_real_connect(mysql,host,user,pointer_to_passwd,NULL,port,NULL,0)) {
    fprintf(stderr, "%s: Failed to connect to database: Error: %s\n",ID,
	    mysql_error(mysql));
    return ECONNABORTED;
  } else {

    /* Now we'll try to load the correct database */
    if(!database){
      fprintf(stderr,"%s: Null database in %s:%d. This should NEVER happen!\n",ID,__FILE__,__LINE__);
      return -1;
    }

    buffer = (char *)malloc(sizeof(char)*(strlen(database)+strlen("CREATE DATABASE IF NOT EXISTS ")+2));      
    sprintf(buffer,"CREATE DATABASE IF NOT EXISTS %s;",database);
    if( mysql_query(mysql,buffer) ){
      fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
      return -1;
    }
    free(buffer);      

    if( mysql_select_db(mysql,database) ){
      /* There is no way, at least that i know of, to check if the action was refused
       * by lack of priviledges. So i'm just using the error value */
      fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
      return -1;
    }
    /*    printf("Connected...\n");*/
  }  
  return 0;
}

/*
 * This function is used to clean up all the mess before quiting
 */
int close_connection(MYSQL * mysql){
  mysql_close(mysql); 
  /*  printf("Connection closed.\n");*/
  return 0;
}
  

/*
 * Compares mysql DATETIME values
 *
 * Return value:
 * 
 * Return 1 is datetime1 is greater than datetime2, 0 is they are equal 
 * and 2 if datetime2 is greater than datetime1. In case of error returns -1 and sets errno.
 */
int compare_datetimes( char * datetime1, char * datetime2){
  int year1,year2;
  int month1,month2;
  int day1,day2;
  int hour1,hour2;
  int minute1,minute2;
  int second1,second2;
  if(!datetime1 || !datetime2){
    fprintf(stderr,"%s: Passing null pointer as string in %s:%d\n",ID,__FILE__,__LINE__);
    return -1;
    errno = 22;
  }
  sscanf(datetime1,"%d-%d-%d %d:%d:%d",&year1,&month1,&day1,&hour1,&minute1,&second1);
  sscanf(datetime2,"%d-%d-%d %d:%d:%d",&year2,&month2,&day2,&hour2,&minute2,&second2);
  if(year1 > year2){
    return 1;
  } else if(year2 > year1){
    return 2;
  } else{
    if(month1 > month2){
      return 1;
    } else if(month2 > month1){
      return 2;
    } else {
      if(day1 > day2){
	return 1;
      } else if (day2 > day1){
	return 2;
      }	else {
	if(hour1 > hour2){
	  return 1;
	} else if(hour2 > hour1){
	  return 2;
	} else {
	  if(minute1 > minute2){
	    return 1;
	  } else if (minute2 > minute1) {
	    return 2;
	  } else {
	    if (second1 > second2){
	      return 1;
	    } else if( second2 > second1){
	      return 2;
	    } else {
	      return 0;
	    }
	  }
	}
      }
    }
  }
  fprintf(stderr,"I hopefully won't ever read this from stderr. Error in %s:%d\n",__FILE__,__LINE__);
  return -1;
}

char ** get_package_list(MYSQL * mysql){
  char * buffer;
  MYSQL_RES  * res;
  MYSQL_ROW row;
  char ** result;
  int num_rows;
  int i;
  if(mysql_query(mysql,"SELECT package_name FROM main;")){
    fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
    fprintf(stderr,"buffer - %s\n",buffer);
    return NULL;
  }
  res = mysql_store_result(mysql);
  num_rows = mysql_num_rows(res);
  result = (char **)malloc(sizeof(char *)*(num_rows+1));
  for (i = 0;i<num_rows;i++){    
    row = mysql_fetch_row(res);
    result[i] = (char*)malloc(sizeof(char)*(strlen(row[0])+1));
    strcpy(result[i],row[0]);
  }
  result[num_rows] = 0;
  mysql_free_result(res);
  return result;
}
      
int consistency_check(MYSQL * mysql,char ** package_list){
  char * buffer;
  int i,j;
  int num_strings;
  MYSQL_RES  * res;
  MYSQL_ROW row;
  MYSQL_RES * table_exists_res;
  MYSQL_ROW table_exists_row;
  /* If there are no user specified packages we'll check them all */
  if(packages_to_check[0] == NULL){
    packages_to_check = package_list;
  }

  for ( i = 0;packages_to_check[i];i++){
    /* Let's check if the package exists */

    /* Now we have to transform all '-' and '.' into '_' because of mysql 
     * table name restrictions
     * There might be more restriction that can cause problems
     * that i'm not aware of.
     */
    for(j = 0;j<(int)strlen(packages_to_check[i]);j++){
      if(packages_to_check[i][j] == '-' || packages_to_check[i][j] == '.'){
	packages_to_check[i][j] = '_';
      }
    }

    table_exists_res = mysql_list_tables(mysql,packages_to_check[i]);
    table_exists_row = mysql_fetch_row(table_exists_res);
    if(row){
      fprintf(file,"\n\n** Inconsistencies from package %s **\n\n",packages_to_check[i]);
      /* Let's get all the strings from this package */
      buffer = (char *)malloc(sizeof(char)*(strlen("SELECT string FROM ")+strlen(packages_to_check[i])+strlen(ORIGINAL_SUF)+2));
      if(!buffer){
	perror(ID);
	return errno;
      } 
      sprintf(buffer,"SELECT string FROM %s%s;",packages_to_check[i],ORIGINAL_SUF);
      if(mysql_query(mysql,buffer)){
	fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
	fprintf(stderr,"buffer - %s\n",buffer);
	return -1;
      }
      free(buffer);
      res = mysql_store_result(mysql);
      num_strings = mysql_num_rows(res);
      for (j = 0;j<num_strings;j++){
	/* Let's search different translations of the string in every package */
	row = mysql_fetch_row(res);
	check_string(mysql,package_list,row[0]);
      }
      mysql_free_result(res);
    } else {
      fprintf(stderr,"%s: package %s doesn't exist in database\n",ID,packages_to_check[i]);
    }
    mysql_free_result(table_exists_res);
  }
  return 0;
}

int check_string(MYSQL * mysql,char ** package_list,char * string){
  MYSQL_RES * res;
  MYSQL_ROW row;
  char * buffer;
  int i,j;
  /* First we need to make string safe */
  buffer = make_safe_for_insertion(string);
  string = buffer;

  for (i = 0;package_list[i];i++){
    buffer = (char*)malloc(sizeof(char)*(strlen("SELECT string_id FROM ")+strlen(package_list[i])+strlen(ORIGINAL_SUF)+\
					 strlen(" WHERE string = '")+strlen(string)+3));
    if(!buffer){
      perror(ID);
      return errno;
    }
    sprintf(buffer,"SELECT string_id FROM %s%s WHERE string = '%s';",package_list[i],ORIGINAL_SUF,string);
    if(mysql_query(mysql,buffer)){
      fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
      fprintf(stderr,"buffer - %s\n",buffer);
      return errno;
    }
    free(buffer);
    res = mysql_store_result(mysql);
    while(1){
      row = mysql_fetch_row(res);
      if(row && row[0]){
	check_string_id(mysql,package_list[i],row[0]);
      } else {
	break;
      }
    }
    mysql_free_result(res);
  }
  print_results(mysql);

  /* Now let's empty the vector */
  j = different_translations.size();
  for(i = 0;i<j;i++){
    different_translations.pop_back();
    different_packages.pop_back();
  }

  return 0;
}

int print_header(MYSQL * mysql,char * package_name,int string_id){
  char * buffer;
  MYSQL_RES * res;
  MYSQL_ROW row;
  buffer = (char *) malloc(sizeof(char)*(strlen("SELECT string FROM ")+strlen(package_name)+strlen(ORIGINAL_SUF)+\
					 strlen(" WHERE string_id = ")+strlen("-2123123123")+strlen(";")+1));
  if(!buffer){
    perror(ID);
    return errno;
  }
  sprintf(buffer,"SELECT string FROM %s%s WHERE string_id = %d;",package_name,ORIGINAL_SUF,string_id);
  if(mysql_query(mysql,buffer)){
    fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
    fprintf(stderr,"buffer - %s\n",buffer);
    return errno;
  }
  free(buffer);
  res = mysql_store_result(mysql);
  row = mysql_fetch_row(res);
  fprintf(file,"\nDifferent translations of the msgstr:\n");
  fprintf(file,"%s",row[0]);
  mysql_free_result(res);
  return 0;
}

int check_string_id(MYSQL * mysql,char * package_name,char * string_id){
  char * buffer;
  int results;
  MYSQL_RES * res;
  MYSQL_ROW row;
  buffer = (char *)malloc(sizeof(char)*(strlen("SELECT string FROM ")+strlen(package_name)+\
					strlen(" WHERE string_id = ")+strlen(string_id)+strlen(" AND idiom = '")+strlen(idiom)+3));
  if(!buffer){
    perror(ID);
    return errno;
  }
  sprintf(buffer,"SELECT string FROM %s WHERE string_id = %s and idiom = '%s';",package_name,string_id,idiom);
  if(mysql_query(mysql,buffer)){
    fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
    fprintf(stderr,"buffer - %s\n",buffer);
    return errno;
  }
  free(buffer);
  res = mysql_store_result(mysql);
  results = mysql_num_rows(res);
  row = mysql_fetch_row(res);
  if(row){
    buffer = (char *)malloc(strlen(row[0])+1);
    strcpy(buffer,row[0]);
    different_translations.push_back(buffer);
    buffer = (char *)malloc(strlen(package_name)+1);
    strcpy(buffer,package_name);
    different_packages.push_back(buffer);
    header_package_name = buffer;
    header_string_id = atoi(string_id);
  }
  mysql_free_result(res);
  return 0;
}


void print_results(MYSQL * mysql){
  int i,j;
  char flag = 0;
  /* We'll only print if there are 2 different alternatives */
  for(i = 0;i<(int)different_translations.size();i++){
    if(strcmp(different_translations[0],different_translations[i])){
      /* There are at least 2 alternatives */
      flag = 1;
    }
  }
  if(flag){
    print_header(mysql,header_package_name,header_string_id);
    for(i = 0;i<(int)different_translations.size();i++){
      if(different_translations[i] != NULL){
	fprintf(file,"From %s",different_packages[i]);
	/*Let's check for similar translations and set them to NULL */
	for(j = i+1;j<(int)different_translations.size();j++){
	  if(different_translations[j]){
	    if(!strcmp(different_translations[i],different_translations[j])){
	      fprintf(file,",%s",different_packages[j]);
	      free(different_translations[j]);
	      different_translations[j] = NULL;
	    }
	  }
	}
	fprintf(file,"\n%s",different_translations[i]);
      }
    }
  }
  for(i = 0;i<(int)different_translations.size();i++){
    if(different_translations[i] != NULL){
      free(different_translations[i]);
      different_translations[i] = NULL;
    }
    if(different_packages[i] != NULL){
      free(different_packages[i]);
      different_packages[i] = NULL;
    }
  }
}
	      
	   

/*
 * Transforms a string to make it mysql safe
 *
 * Return value:
 *
 * Returns the mysql safe for insertion string or NULL if it fails. 
 * If it fails errno is set.
 */

char * make_safe_for_insertion(char * string){
  int i,j;
  int unsafe_chars;
  char * result;
  if (!string){
    errno = 22;
    perror(ID);
    return NULL;
  }
  unsafe_chars = 0;
  for( i = 0;string[i] != '\0';i++){
    if (string[i] == 39 || string[i] == '"' || string[i] == '\\' || string[i] == '\'' ){
      unsafe_chars++;
    }
  }
  result = (char *)malloc(sizeof(char)*(strlen(string)+unsafe_chars+1));
  if(!result){
    perror(ID);
    return NULL;
  }
  for( i = 0, j = 0;string[i] != '\0';i++,j++){
    if (string[i] == 39 || string[i] == '"' || string[i] == '\\' || string[i] == '\''){
      /* We must place a escape character before for correct interpretation */
      result[j] = '\\';
      j++;
    }
    result[j] = string[i];
  }
  /* Close the string */
  result[strlen(string)+unsafe_chars] = '\0';
  return result;
}
