/*  Copyright (c) 2002 Filipe Maia <fmaia@gmx.net>. All rights reserved.   *
 *                                                                         *
 * Permission is hereby granted, free of charge, to any person obtaining a *
 * copy of this software and associated documentation files (the           *
 * "Software"), to deal in the Software without restriction, including     *
 * without limitation the rights to use, copy, modify, merge, publish,     *
 * distribute, and/or sell copies of the Software, and to permit persons   *
 * to whom the Software is furnished to do so, provided that the above     *
 * copyright notice(s) and this permission notice appear in all copies of  *
 * the Software and that both the above copyright notice(s) and this       *
 * permission notice appear in supporting documentation.                   *
 *                                                                         *              
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS * 
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF              *
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT   *
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR        *
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL *
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING  *
 * FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,    *
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION    *
 * WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.                           */

#ifdef __cplusplus
extern "C" {
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <mysql.h>
#include <getopt.h>
#include <termios.h>
#include <time.h>

  char *strptime(const char *s, const char  *format,  struct
		 tm *tm);

}
#endif /* __cplusplus */


/*
 * I will call original table to the table holding the
 * untranslated entries of one package
 */

#define _XOPEN_SOURCE
#define _GNU_SOURCE

#define ID "po2mysql"
#define VERSION "0.0.2"
#define POT_EXTENSION ".pot"
#define PO_EXTENSION ".po"
#define DEFAULT_HOST "localhost"
#define DEFAULT_DB "gnome_translations"
#define MSGID_IDIOM "en"
#define MAIN_TABLE "main"
#define ORIGINAL_SUF "_orig"

#define LOCATION_BUFFER 40960
#define COMMENT_BUFFER 40960
#define MSGID_BUFFER 40960
#define MSGSTR_BUFFER 40960

#define POT_CREATION_DATE_TAG "\"POT-Creation-Date:"
#define PO_REVISION_DATE_TAG "\"PO-Revision-Date:"
#define LAST_TRANSLATOR_TAG "\"Last-Translator:"
#define COMMENT_TAG "#."
#define COMMENT_TRANSLINEATION_MARK '*'
#define SOURCE_LOCATION_TAG "#:"
#define MSGSTR_TAG "msgstr"
#define MSGID_TAG "msgid"
#define IN_MSGSTR 2
#define IN_MSGID 1
#define IN_WILDERNESS 0

#define BEFORE_MSG 0
#define INSIDE_MSG 1
#define AFTER_MSG 2

#define NULL_MSGSTR "\\\"\\\"\n"
int main(int argc,char **argv);
int init(MYSQL ** new_db);
int parse_options(int argc,char ** argv);
int connect(MYSQL * pointer_to_db);
int check_main(MYSQL * mysql);
int process_files(MYSQL * mysql);
int process_pot(MYSQL * mysql,char * file, int index);
int process_po(MYSQL * mysql,char * file, int index);
int check_in_package(MYSQL *  mysql,char * package_name);
int po_to_table(MYSQL * mysql,FILE * file,char * package_name,int package_id,char * idiom);
int pot_to_table(MYSQL * mysql,FILE * file,char * package_name,int package_id);
int get_next_message(FILE * file,char ** comment,char ** location,char ** msgid,char ** msgstr);
void usage();
void version();
int close_connection(MYSQL * mysql);
int compare_datetimes(char * datetime1, char * datetime2);
char * make_safe_for_insertion(char * string);
char * convert_to_GMT(char * string);
int insert_in_original(MYSQL * mysql,char * package_name,int package_id,char * creation,char * msgid, char * location,char  * notes);
int insert_in_translations(MYSQL * mysql,char * package_name, int package_id,int string_id,\
			   char * creation, char * revision, char * msgstr, char * idiom, char * notes);

int update_original(MYSQL * mysql,char * package_name,char * creation,int string_id);
int update_translation(MYSQL * mysql,char * package_name,char * revision, char * msgstr, int string_id);

int update_creation_in_translations(MYSQL * mysql,char * package_name,char * creation,int string_id);

int search_string_id_in_original(MYSQL * mysql, char * package_name,char * msgid,char * location,char * notes);
int string_exists_in_translated(MYSQL * mysql,char * package_name,int string_id,char * idiom);
