/*  Copyright (c) 2002 Filipe Maia <fmaia@gmx.net>. All rights reserved.   *
 *                                                                         *
 * Permission is hereby granted, free of charge, to any person obtaining a *
 * copy of this software and associated documentation files (the           *
 * "Software"), to deal in the Software without restriction, including     *
 * without limitation the rights to use, copy, modify, merge, publish,     *
 * distribute, and/or sell copies of the Software, and to permit persons   *
 * to whom the Software is furnished to do so, provided that the above     *
 * copyright notice(s) and this permission notice appear in all copies of  *
 * the Software and that both the above copyright notice(s) and this       *
 * permission notice appear in supporting documentation.                   *
 *                                                                         *              
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS * 
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF              *
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT   *
 * OF THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR        *
 * HOLDERS INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL *
 * INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING  *
 * FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,    *
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION    *
 * WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.                           */


/*
 * Global Variables
 */

/*
 * They are just used for the store parameters 
 * TODO:I should probably remove them
 */

int port; 
char * user; 
char use_passwd;
char * host;
char ** files;
int number_of_files;
char * database;

/*
 * Includes
 */

#include "po2mysql.h"

/*
 * Functions
 * 
 * Return values:
 * 
 * If no comment about the return value of a function is made
 * they are assumed to return 0 if succesful, errno for a system error,
 * and -1 for a mysql error
 */

int main(int argc,char *argv[]){
  int ret;
  MYSQL * mysql;
  ret = init(&mysql);
  if (ret){
    return ret;
  }
  ret = parse_options(argc,argv);
  if (ret){
    return ret;
  }
  ret = connect(mysql);
  if (ret){
    return ret;
  }
  ret = check_main(mysql);
  if (ret) {
    return ret;
  }
  ret = process_files(mysql);
  if (ret){
    return ret;
  }
  ret = close_connection(mysql);
  if (ret){
    return ret;
  }
  return 0;
}

/*
 * Initializes the global variables and the MYSQL structure
 */
int init(MYSQL ** new_db){
  MYSQL * mysql;
  /* I'm not sure if mysql_init sets errno so i'm setting it manually */
  mysql = mysql_init(0);  
  if (!mysql) {
    errno = 12;
    perror(ID);
    return errno;
  }
  use_passwd = 0;
  port = 0;
  user = NULL;
  files = NULL;
  number_of_files = 0;
  database = NULL;
  host = (char *)malloc(sizeof(char) * (strlen(DEFAULT_HOST)+1));
  if (!host) {
    perror(ID);
    return errno;
  }  
  strcpy(host,DEFAULT_HOST);
  database = (char *)malloc(sizeof(char) * (strlen(DEFAULT_DB)+1));
  if (!database) {
    perror(ID);
    return errno;
  }  
  strcpy(database,DEFAULT_DB);
  *new_db = mysql;
  return 0;
}

/*
 * Sets the global variables according to the options passed
 */
int parse_options(int argc,char ** argv){
  char c;
  int i;
  static struct option long_options[] = {
    {"passwd", 0, 0, 'p'},
    {"host", 1, 0, 'H'},
     {"port", 1, 0, 'P'},
    {"user", 1, 0, 'u'},
    {"help", 0, 0, 'h'},
    {"version", 0, 0, 'v'},    
    {"database", 1, 0, 'D'},
    {0, 0, 0, 0}
  };

  while((c = getopt_long(argc,argv,"vphD:H:P:u:",long_options,NULL)) != -1){
    switch(c){
    case 'p':
      use_passwd = 1;
      break;
    case 'H':
      if(optarg && strlen(optarg)){
	free(host);
	host = (char *)malloc(sizeof(char)*strlen(optarg)+1);
	strcpy(host,optarg);
      } else {
	fprintf(stderr,"%s: Passing null pointer as argument in %s:%d\n",ID,__FILE__,__LINE__);
      }
      break;
    case 'P':
      if(optarg && strlen(optarg)){
	errno = 0;
	port = strtol(optarg,NULL,10);
	if (errno){
	  perror(ID);
	  port = 0;
	}
      } else {
	fprintf(stderr,"%s: Passing null pointer as argument in %s:%d\n",ID,__FILE__,__LINE__);
      }    
      break;
    case 'u':
      if(optarg && strlen(optarg)){
	user = (char *)malloc(sizeof(char)*strlen(optarg)+1);
	strcpy(user,optarg);
      } else {
	fprintf(stderr,"%s: Passing null pointer as argument in %s:%d\n",ID,__FILE__,__LINE__);
      }
      break;
    case 'D':
      if(optarg && strlen(optarg)){
	free(database);
	database = (char *)malloc(sizeof(char)*strlen(optarg)+1);
	strcpy(database,optarg);
      } else {
	fprintf(stderr,"%s: Passing null pointer as argument in %s:%d\n",ID,__FILE__,__LINE__);
      }
      break;
    case 'h':
      usage();
      exit(0);
      break;
    case 'v':
      version();
      exit(0);
      break;
    default:
      fprintf(stderr,"%s: Error parsing options\n",ID);
    }
  }

  /*
   * Assumes the rest of the options are the files to insert in the database
   * and sets the global list char ** files accordingly
   */
  files = (char **)malloc(sizeof(char *)*(argc-optind+1));
  if(!files){
    perror(ID);
    return errno;
  }
  files[argc-optind] = NULL;
  for (i = optind;i < argc;i++) {
    files[i-optind] = (char *)malloc(sizeof(char)*strlen(argv[i])+1);
    if(!files[i-optind]){
      perror(ID);
      return errno;
    }
    strcpy(files[i-optind],argv[i]);
    number_of_files++;
  }
  if(number_of_files == 0){
    fprintf(stderr,"%s: You need to specify a file. See %s --help\n",ID,ID);
    return -1;
  }
  return 0;
}  

/*
 * Creates the 'index' table if needed
 */
int check_main(MYSQL * mysql){
  char * buffer;
  buffer = (char *)malloc(sizeof(char)*(strlen("CREATE TABLE IF NOT EXISTS ")+strlen(MAIN_TABLE)+ \
					strlen("(package_name VARCHAR(100), package_id INT NOT NULL AUTO_INCREMENT, PRIMARY KEY (package_id));")+1));
  if(!buffer){
    perror(ID);
    return errno;
  }
  sprintf(buffer,"CREATE TABLE IF NOT EXISTS %s(package_name VARCHAR(100), package_id INT NOT NULL AUTO_INCREMENT, PRIMARY KEY (package_id));",MAIN_TABLE);
  if( mysql_query(mysql,buffer) ){
    fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
    return -1;
  }
  free(buffer);        
  return 0;
}
  


/*
 * Processes the files list
 */
int process_files(MYSQL * mysql){
  int i;
  char * j;
  char ** filenames;
  filenames = (char **)malloc(sizeof(char *)*number_of_files+1);
  if(!filenames){
    perror(ID);
    return errno;
  }
  filenames[number_of_files] = NULL;
  for(i = 0;files[i] != NULL;i++){
    /* Search for the last '/' so we can isolate the filename from the path */    
    for (j = &(files[i][strlen(files[i])]); *j != '/' && j>=files[i];j--);
    filenames[i] = (char *)malloc(sizeof(char)*(&(files[i][strlen(files[i])])-j));
    if(!filenames[i]){
      perror(ID);
      return errno;
    }
    strcpy(filenames[i],j+1);
    /* Now determine if file is a .po or a .pot */
    j = &(filenames[i][strlen(filenames[i])-4]);
    if(!strcmp(j,POT_EXTENSION)){
      process_pot(mysql,filenames[i],i);
    } else if(!strcmp(j+1,PO_EXTENSION)){
      process_po(mysql,filenames[i],i);
    } else {
      fprintf(stderr,"%s: File %s is not of a supported format\n",ID,files[i]);
    }
  }
  return 0;
}


/* 
 * Makes all the needed stuff to the .pot files
 */
int process_pot(MYSQL * mysql,char * file, int index){
  FILE * pot_file;
  char * package_name;
  int i;
  char * buffer;
  int package_id;
  pot_file = fopen(files[index],"r");
  if(!pot_file){
    perror(ID);
    return errno;
  }
  package_name = (char *)malloc(sizeof(char)*strlen(file)+1);
  if(!package_name){
    perror(ID);
    return errno;
  }  
  /* This will remove the extension from the package_name */
  /* In the process we'll waste 4 bytes but i think it's ok */
  strcpy(package_name,file);
  package_name[strlen(file)-4] = '\0';

  /* Now we have to transform all '-' and '.' into '_' because of mysql 
   * table name restrictions
   * There might be more restriction that can cause problems
   * that i'm not aware of.
   */
  for(i = 0;i<(int)strlen(package_name);i++){
    if(package_name[i] == '-' || package_name[i] == '.'){
      package_name[i] = '_';
    }
  }

  /*
   * Now we crete a table for this package if it doesn't already exists 
   */
  buffer = (char *) malloc(sizeof(char)*(strlen("CREATE TABLE IF NOT EXISTS ")+strlen(package_name)+strlen(ORIGINAL_SUF)+ \
					 strlen(" (package_id INT,string_id INT NOT NULL AUTO_INCREMENT,string BLOB,location TEXT,creation DATETIME,hotkey_level TINYINT,notes TEXT,INDEX id_index(string_id),INDEX string_index(string(10));")+1));
  if(!buffer){
    perror(ID);
    return errno;
  }
  sprintf(buffer,"CREATE TABLE IF NOT EXISTS %s%s (package_id INT,string_id INT NOT NULL AUTO_INCREMENT,string BLOB,location TEXT,creation DATETIME,hotkey_level TINYINT,notes TEXT,INDEX id_index(string_id),INDEX string_index(string(10)));",package_name,ORIGINAL_SUF);
  
  if( mysql_query(mysql,buffer) ){
    fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
    return -1;
  }
  free(buffer);        
  
  /*
   * Now let's check if the package already exists in main
   */
  package_id = check_in_package(mysql,package_name);

  /* 
   *Now we have to deal with the special return value 
   */
  if(package_id == 0){
    return -1;
  } else if (package_id < 0){
    return -package_id;
  }
  pot_to_table(mysql,pot_file,package_name,package_id);
  fclose(pot_file);
  return 0;
}

/* 
 * Makes all the needed stuff to the .pot files
 */
int process_po(MYSQL * mysql,char * file, int index){
  FILE * po_file;
  char * package_name;
  int i;
  char * idiom;
  char * buffer;
  int package_id;
  po_file = fopen(files[index],"r");
  if(!po_file){
    perror(ID);
    return errno;
  }
  package_name = (char *)malloc(sizeof(char)*strlen(file)+1);
  if(!package_name){
    perror(ID);
    return errno;
  }  
  /* This will remove the extension from the package_name */
  /* And determine the file idiom */
  strcpy(package_name,file);
  package_name[strlen(file)-3] = '\0';
  for(i = strlen(package_name);i >= 0 && package_name[i] != '-' && package_name[i] != '.' ;i--);
  idiom = (char *)malloc(sizeof(char)*(strlen(package_name)-i));
  strcpy(idiom,&package_name[i+1]);
  package_name[i] = '\0';

  /* Now we have to transform all '-' and '.' into '_' because of mysql 
   * table name restrictions
   * There might be more restriction that can cause problems
   * that i'm not aware of.
   */
  for(i = 0;i<(int)strlen(package_name);i++){
    if(package_name[i] == '-' || package_name[i] == '.'){
      package_name[i] = '_';
    }
  }

  /*
   * Now we crete the tables needed for this package if they don't already exist
   */
  buffer = (char *) malloc(sizeof(char)*(strlen("CREATE TABLE IF NOT EXISTS ")+strlen(package_name)+ \
					 strlen(" (package_id INT,string_id INT,idiom VARCHAR(6),string BLOB,creation DATETIME,revision DATETIME,hotkey_level TINYINT,notes TEXT,INDEX id_index(string_id,idiom),INDEX string_index(string(10)));")+1));
  if(!buffer){
    perror(ID);
    return errno;
  }
  sprintf(buffer,"CREATE TABLE IF NOT EXISTS %s (package_id INT,string_id INT,idiom VARCHAR(6),string BLOB,creation DATETIME,revision DATETIME,hotkey_level TINYINT,notes TEXT,INDEX id_index(string_id,idiom),INDEX string_index(string(10)));",package_name);
  
  if( mysql_query(mysql,buffer) ){
    fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
    fprintf(stderr,"buffer - %s\n",buffer);
    return -1;
  }
  free(buffer);        

  buffer = (char *) malloc(sizeof(char)*(strlen("CREATE TABLE IF NOT EXISTS ")+strlen(package_name)+strlen(ORIGINAL_SUF)+ \
					 strlen(" (package_id INT,string_id INT NOT NULL AUTO_INCREMENT,string BLOB,location TEXT,creation DATETIME,hotkey_level TINYINT,notes TEXT,INDEX id_index(string_id),INDEX string_index(string(10)));")+1));
  if(!buffer){
    perror(ID);
    return errno;
  }
  sprintf(buffer,"CREATE TABLE IF NOT EXISTS %s%s (package_id INT,string_id INT NOT NULL AUTO_INCREMENT,string BLOB,location TEXT,creation DATETIME,hotkey_level TINYINT,notes TEXT,INDEX id_index(string_id),INDEX string_index(string(10)));",package_name,ORIGINAL_SUF);
  
  if( mysql_query(mysql,buffer) ){
    fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
    return -1;
  }
  free(buffer);        

  
  /*
   * Now let's check if the package already exists in main
   */
  package_id = check_in_package(mysql,package_name);

  /* 
   *Now we have to deal with the special return value 
   */
  if(package_id == 0){
    return -1;
  } else if (package_id < 0){
    return -package_id;
  }
  po_to_table(mysql,po_file,package_name,package_id,idiom);
  fclose(po_file);
  return 0;
}

/*
 * Check package_name in MAIN_TABLE
 *
 *  Return Value
 *
 *  WARNING: non-default return value!
 *  Returns the package_id if sucessful, -errno for system error, and 0 for mysql error.
 */
int check_in_package(MYSQL * mysql, char * package_name){
  int package_id;
  char * buffer;
  MYSQL_RES *res;
  MYSQL_ROW row;
  buffer = (char *)malloc(sizeof(char)*(strlen("SELECT * FROM ")+strlen(MAIN_TABLE)+ \
					strlen(" WHERE package_name = '")+strlen(package_name)+3));
  if(!buffer){
    perror(ID);
    return -errno;
  }
  sprintf(buffer,"SELECT * FROM %s WHERE package_name = '%s';",MAIN_TABLE,package_name);
  if( mysql_query(mysql,buffer) ){
    fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
    fprintf(stderr,"buffer - %s\n",buffer);
    return 0;
  }
  free(buffer);        
  res = mysql_store_result(mysql);
  row = mysql_fetch_row(res);
  if(!row){
    /* Package still didn't checked in the main table */

    buffer = (char *)malloc(sizeof(char)*(strlen("INSERT INTO ")+strlen(MAIN_TABLE)+\
					  strlen("(package_name) VALUES ('")+strlen(package_name)+strlen("');")+1));
    if(!buffer){
      perror(ID);
      return -errno;
    }
    sprintf(buffer,"INSERT INTO %s(package_name) VALUES ('%s');",MAIN_TABLE,package_name);
    if( mysql_query(mysql,buffer) ){
      fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
      fprintf(stderr,"buffer - %s\n",buffer);
      return 0;
    }
    free(buffer);        
    package_id = mysql_insert_id(mysql);
  } else {
    /* Package already checked in */
    package_id = atoi(row[1]);
  }    
  mysql_free_result(res);
  return package_id;
}

/*
 * Insert the strings of the po file pointed by the file in the database
 */

/*
 * I've managed to reduce the ugly monster a bit but it's still very big
 */
int po_to_table(MYSQL * mysql,FILE * file,char * package_name,int package_id,char * idiom){
  char * buffer;
  char * aux_buffer;
  char * last_translator;
  char * po_revision_date;
  char * pot_creation_date;
  char * location = NULL;
  char * notes = NULL;
  char * msgid = NULL;
  char * msgstr = NULL;
  int msg_counter;
  int x,string_exists;
  MYSQL_RES * res, * res2,* res3;
  MYSQL_ROW row;
  
  res = res2 = res3 =  NULL;

  buffer = (char *)malloc(sizeof(char)*4096);
  aux_buffer = (char *) malloc(sizeof(char)*4096);
  if(!file){
    fprintf(stderr,"%s: Someone is jocking with me passing NULL file pointers in %s:%d\n",ID,__FILE__,__LINE__);
    return -1;
  }
  if(!package_name || !idiom){
    fprintf(stderr,"%s: Passing NULL pointer as string in %s:%d\n",ID,__FILE__,__LINE__);
    return -1;
  } 
  if(package_id<=0){
    fprintf(stderr,"%s: Passing invalid number a package_id in %s:%d\n",ID,__FILE__,__LINE__);
  }

  /*
   * Let's try to get the information about the POT creation date
   */

  do{
    fgets(buffer,4096,file);
  } while(!strstr(buffer,POT_CREATION_DATE_TAG));
  strcpy(aux_buffer,strstr(buffer,":")+1);
  aux_buffer[strlen(aux_buffer)-4] = '\0';
  pot_creation_date = convert_to_GMT(aux_buffer);
  if(!pot_creation_date){
    return errno;
  }

  /*
   * Let's try to get the information about the PO creation date
   */

  do{
    fgets(buffer,4096,file);
  } while(!strstr(buffer,PO_REVISION_DATE_TAG));
  strcpy(aux_buffer,strstr(buffer,":")+1);
  aux_buffer[strlen(aux_buffer)-4] = '\0';
  po_revision_date = convert_to_GMT(aux_buffer);
  if(!po_revision_date){
    return errno;
  }

  /*
   * Let's try to get the information about the last translator
   */
  do{
    fgets(buffer,4096,file);
  } while(!strstr(buffer,LAST_TRANSLATOR_TAG));
  strcpy(aux_buffer,strstr(buffer,":"));
  aux_buffer[strlen(aux_buffer)-4] = '\0';
  last_translator = (char *)malloc(sizeof(char)*strlen(buffer)+1);

  if(!last_translator){
    perror(ID);
    return errno;
  }
  strcpy(last_translator,aux_buffer);

  /*
   * Let's see if the data stored is older than this one
   */

  free(buffer);

  buffer = (char *)malloc(sizeof(char)*(strlen("SELECT MAX(revision) FROM ")+strlen(package_name)+strlen("  WHERE idom = '")+\
					+strlen(idiom)+strlen("';")+1));
  if(!buffer){
    perror(ID);
    return errno;
  }
  sprintf(buffer,"SELECT MAX(revision) FROM %s WHERE idiom = '%s';",package_name,idiom);

  if( mysql_query(mysql,buffer) ){
    fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
    return 0;
  }
  free(buffer);
  res = mysql_store_result(mysql);
  row = mysql_fetch_row(res);
  if(!row[0] || (compare_datetimes(row[0],po_revision_date) == 2) ){
    /* If've got something new */

    /*
     * Let's try to find out our string_id
     * We'll search in package_nameORIGINAL_SUF table
     */
    buffer = (char *)malloc(sizeof(char)*(strlen("SELECT MAX(creation) FROM ;")+strlen(package_name)+strlen(ORIGINAL_SUF)+1));
    if(!buffer){
      perror(ID);
      return errno;
    }
    sprintf(buffer,"SELECT MAX(creation) FROM %s%s;",package_name,ORIGINAL_SUF);
    
    if( mysql_query(mysql,buffer) ){
      fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
      return 0;
    }
    free(buffer);
    res2 = mysql_store_result(mysql);
    row = mysql_fetch_row(res2);
    if(!row[0]){
      /* The package_nameORIGINAL_SUF table is also clean */
      msg_counter = 0;
      
      do{
	if(get_next_message(file,&notes,&location,&msgid,&msgstr)){
	  return errno;
	}
	if (!msgid){      
	  /* We're in the end */      
	  break;
	}	
	msg_counter++;
	insert_in_original(mysql,package_name,package_id,pot_creation_date,msgid,location,notes);
	
	if(strcmp(msgstr,NULL_MSGSTR)){
	  /* We'll not insert null msgstr */	  
	  insert_in_translations(mysql,package_name,package_id,msg_counter,pot_creation_date,po_revision_date,msgstr,idiom,notes);
	}
	
      } while(msgid!=NULL);

    }  else if ( compare_datetimes(row[0],pot_creation_date) == 2){
    /* The pot from which this file was created is more recent than the last 
     * placed in the database. Let's update the package_nameORIGINAL_SUF table.
     *
     * This can take a while
     */
      do{
	if(get_next_message(file,&notes,&location,&msgid,&msgstr)){
	  return errno;
	}
	if (!msgid){
	  /* We're in the end */
	  break;
	}
	msg_counter++;

	buffer = (char *)malloc(sizeof(char)*(strlen("SELECT string_id,creation FROM ")+strlen(package_name)+strlen(ORIGINAL_SUF)+\
					      strlen(" WHERE string = '")+strlen(msgid)+strlen("' AND location = '")+\
					      strlen(location)+strlen("' AND notes = '")+strlen(notes)+strlen("';")+1));
	if(!buffer){
	  perror(ID);
	  return errno;
	}
	sprintf(buffer,"SELECT string_id,creation FROM %s%s WHERE string = '%s' AND location = '%s' and notes = '%s';",package_name,\
		ORIGINAL_SUF,msgid,location,notes);
	if( mysql_query(mysql,buffer) ){
	  fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
	  fprintf(stderr,"buffer - %s\n",buffer);
	  return 0;
	}
	free(buffer);
	res3 = mysql_store_result(mysql);
	row = mysql_fetch_row(res3);
	if(!row[0]){
	  /* No such message in original table. Let's add it */		
	  insert_in_original(mysql,package_name,package_id,pot_creation_date,msgid,location,notes);

	  msg_counter = mysql_insert_id(mysql);
	  if(strcmp(msgstr,NULL_MSGSTR)){
	  /*We'll not insert null msgstr */	    
	    insert_in_translations(mysql,package_name,package_id,msg_counter,pot_creation_date,po_revision_date,msgstr,idiom,notes);
	  }
	  /*
	  free(msgid);
	  free(location);
	  free(notes);
	  free(msgstr);
	  */
	} else if (compare_datetimes(row[1],pot_creation_date) == 2){
	  /* The string exists but our date is more recent. Let's just update the date */
	  msg_counter = atoi(row[0]);
	  
	  update_original(mysql,package_name,pot_creation_date,msg_counter);
	  /* Now we need to take care of the package_name table 
	   *
	   * If the strings exists in the table we need to update the creation and revision date
	   * If it doesn't we need to create it
	   */
	  string_exists = string_exists_in_translated(mysql,package_name,msg_counter,idiom);

	  if(!string_exists){
	    if(strcmp(msgstr,NULL_MSGSTR)){
	      /* We won't add NULL msgstr */
	      /* The message doesn't exists in the po table. Let's add it*/
	      insert_in_translations(mysql,package_name,package_id,msg_counter,pot_creation_date,po_revision_date,msgstr,idiom,notes);
	    }
	  } else {
	    /* The message exists but needs to be updated */
	    update_translation(mysql,package_name,po_revision_date,msgstr,msg_counter);
	  }
	}
      } while(msgid!=NULL);	
    } else {
      /* The pot file is at least as good as ours so the messages should be up to date 
       * We'll just limit to add new messages 
       */
      do{
	/*
	 * Now let's ge the new message
	 */
	if(get_next_message(file,&notes,&location,&msgid,&msgstr)){
	  return errno;
	}
	if (!msgid){
	  /* We're in the end */
	  break;
	}
	msg_counter = search_string_id_in_original(mysql,package_name,msgid,location,notes);
	x = string_exists_in_translated(mysql,package_name,msg_counter,idiom);
	if(!x){
	  /* The message doesn't exists in the po table. Let's add it*/
	  if(strcmp(msgstr,NULL_MSGSTR)){
	    /* We won't insert NULL msgstr */
	    insert_in_translations(mysql,package_name,package_id,msg_counter,pot_creation_date,po_revision_date,msgstr,idiom,notes);
	  }
	} else {
	  /* The message already exists in the po table but
	   * our revision date is more recent 
	   * Let's update the row according to our information
	   */
	  update_translation(mysql,package_name,po_revision_date,msgstr,msg_counter);
	}
      } while(msgid!=NULL);	
    }
  }
  if(res){
    free(res);
  }
  if(res2){
    free(res2);
  } 
  if(res3){    
    free(res3);
  } 
  return 0;
}


/*
 * Insert the strings of the pot file pointed by the file in the database
 */
int pot_to_table(MYSQL * mysql, FILE * file,char * package_name,int package_id){
  char * buffer;
  char * aux_buffer;
  char * creation_date;
  char * location;
  char * notes;
  char * msgid;
  int msg_counter;
  int string_exists;
  MYSQL_RES * res, * res3;
  MYSQL_ROW row;

  location = notes = msgid = NULL;
  
  buffer = (char *)malloc(sizeof(char)*4096);
  aux_buffer = (char *) malloc(sizeof(char)*4096);
  if(!buffer  || !aux_buffer){
    perror(ID);
    return errno;
  }

  if(!file){
    fprintf(stderr,"%s: Someone is jocking with me passing NULL file pointers in %s:%d\n",ID,__FILE__,__LINE__);
    return -1;
  }
  if(!package_name){
    fprintf(stderr,"%s: Passing NULL pointer as string in %s:%d\n",ID,__FILE__,__LINE__);
    return -1;
  } 
  if(package_id<=0){
    fprintf(stderr,"%s: Passing invalid number a package_id in %s:%d\n",ID,__FILE__,__LINE__);
  }

  /*
   * Let's try to get the information about the creation date
   */
  do{
    fgets(buffer,4096,file);
  } while(!strstr(buffer,POT_CREATION_DATE_TAG));
  strcpy(aux_buffer,strstr(buffer,":")+1);
  aux_buffer[strlen(aux_buffer)-4] = '\0';
  creation_date = convert_to_GMT(aux_buffer);
  if(!creation_date){
    return errno;
  }

  /*
   * Let's see if the data stored is older than this one
   */
  
  buffer = (char *)malloc(sizeof(char)*(strlen("SELECT MAX(creation) FROM ;")+strlen(package_name)+strlen(ORIGINAL_SUF)+1));
  if(!buffer){
    perror(ID);
    return errno;
  }
  sprintf(buffer,"SELECT MAX(creation) FROM %s%s;",package_name,ORIGINAL_SUF);

  if( mysql_query(mysql,buffer) ){
    fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
    return 0;
  }
  free(buffer);
  res = mysql_store_result(mysql);
  row = mysql_fetch_row(res);
  if(!row[0]){
    /* There is no data stored yet, we're safe */
    msg_counter = 0;
    do{
      
      if(get_next_message(file,&notes,&location,&msgid,&buffer)){
	return errno;
      }
      if (!msgid){
	/* We're in the end */
	break;
      }
      msg_counter++;
      insert_in_original(mysql,package_name,package_id,creation_date,msgid,location,notes);
      /*
      free(buffer);
      free(msgid);
      free(location);
      free(notes);
      */
    } while(msgid!=NULL);
  } else if (compare_datetimes(row[0],creation_date) == 2){
    /* This .pot file is more recent than the last stored.
     * This can take some time
     */
    msg_counter = 0;
    do{
      if(get_next_message(file,&notes,&location,&msgid,&buffer)){
	return errno;
      }
      if (!msgid){
	/* We're in the end */
	break;
      }      
      msgid = aux_buffer;
      msg_counter++;

      string_exists = search_string_id_in_original(mysql,package_name,msgid,location,notes);
      if(string_exists < 0){
	return errno;
      }
      
      if(!string_exists){
	/* There's no such string let's add it */
	insert_in_original(mysql,package_name,package_id,creation_date,msgid,location,notes);
      } else {
	/* There already is string like this */
	/* Let's just update the date */
	
	update_original(mysql,package_name,creation_date,string_exists);

	update_creation_in_translations(mysql,package_name,creation_date,string_exists);

      }
      /*
      free(buffer);      
      free(msgid);
      free(location);
      free(notes);
      */
      mysql_free_result(res3);
    } while(msgid!=NULL);
  } else {
  }
  mysql_free_result(res);
  return 0;
}


int get_next_message(FILE * file,char ** comment,char ** location,char ** msgid,char ** msgstr){
  char * buffer;
  char * aux_buffer;

  char * location_buffer;
  int location_buffer_size;
  char * comment_buffer;
  int comment_buffer_size;
  char * msgid_buffer;
  int msgid_buffer_size;
  char * msgstr_buffer;
  int msgstr_buffer_size;
  int flag;
  int aux_flag;
  
  if(*comment){
    free(*comment);
  }
  if(*location){
    free(*location);
  }
  if(*msgid){
    free(*msgid);
  }
  if(*msgstr){
    free(*msgstr);
  }

  if(!file){
    fprintf(stderr,"%s: Someone is jocking with me passing NULL file pointers in %s:%d\n",ID,__FILE__,__LINE__);
    return -1;
  }

  buffer = (char *)malloc(sizeof(char)*4096);
  aux_buffer = (char *) malloc(sizeof(char)*4096);

  comment_buffer = (char *) malloc(sizeof(char)*COMMENT_BUFFER);
  location_buffer = (char *) malloc(sizeof(char)*LOCATION_BUFFER);
  msgid_buffer = (char *) malloc(sizeof(char)*MSGID_BUFFER);
  msgstr_buffer = (char *) malloc(sizeof(char)*MSGSTR_BUFFER);

  if(!comment_buffer || !aux_buffer || !buffer || !location_buffer || !msgid_buffer || !msgstr_buffer){
    perror(ID);
    return errno;
  }

  /* Set arguments to NULL */
  *location  = NULL;
  *msgid = NULL;
  *msgstr = NULL;
  *comment = NULL;

  /* Init local variables */
  aux_flag = BEFORE_MSG;
  flag = IN_WILDERNESS;
  comment_buffer[0] = '\0';
  location_buffer[0] = '\0';
  msgid_buffer[0] = '\0';
  msgstr_buffer[0] = '\0';
  comment_buffer_size = 1;
  location_buffer_size = 1;
  msgid_buffer_size = 1;
  msgstr_buffer_size = 1;

  while(aux_flag != AFTER_MSG){
    if(!fgets(buffer,4096,file)){
      if (feof(file)){
	/* Something was read */
	if(msgid_buffer[0] != '\0'){
	  *location = make_safe_for_insertion(location_buffer);
	  *comment = make_safe_for_insertion(comment_buffer);
	  *msgid = make_safe_for_insertion(msgid_buffer);
	  *msgstr = make_safe_for_insertion(msgstr_buffer);
	  
	  free(buffer);
	  free(aux_buffer);
	  free(location_buffer);
	  free(comment_buffer);
	  free(msgid_buffer);
	  free(msgstr_buffer);
	}
	return 0;
      } else {
	perror(ID);
	return errno;
      }
    }
    aux_buffer[0] = '\0';
    sscanf(buffer,"%s",aux_buffer);
    if(!strcmp(aux_buffer,COMMENT_TAG)){
      /* We're inside a comment boys */
      if (flag == IN_MSGSTR){
	/* We're in the end of a message */
	flag = IN_WILDERNESS;      
	aux_flag = AFTER_MSG;
	/* Let's move the file pointer back to where it should be */
	if(fseek(file,-strlen(buffer),SEEK_CUR)){
	  perror(ID);
	  return errno;
	}
      }
      if (aux_flag == BEFORE_MSG){
	aux_flag = INSIDE_MSG;
      }
      strcpy(aux_buffer,strstr(buffer,COMMENT_TAG)+strlen(COMMENT_TAG)+1);

      comment_buffer_size += strlen(aux_buffer);
      if(comment_buffer_size < COMMENT_BUFFER){
	strcat(comment_buffer,aux_buffer);
      } else {
	fprintf(stderr,"%s: Not enough room in buffer comment_buffer in %s:%d. Aborting\n",ID,__FILE__,__LINE__);
	errno = ENOBUFS;
	return errno;
      }

    } else if (!strcmp(aux_buffer,SOURCE_LOCATION_TAG)){
      /* Inside a source reference */
      if (flag == IN_MSGSTR){
	/* We're in the end of a message */
	flag = IN_WILDERNESS;      
	aux_flag = AFTER_MSG;
	/* Let's move the file pointer back to where it should be */
	if(fseek(file,-strlen(buffer),SEEK_CUR)){
	  perror(ID);
	  return errno;
	}
      }
      if (aux_flag == BEFORE_MSG){
	aux_flag = INSIDE_MSG;
      }

      strcpy(aux_buffer,strstr(buffer,SOURCE_LOCATION_TAG)+strlen(SOURCE_LOCATION_TAG)+1);
	/*         Overflow WARNING 
	*
	* These strcat are the problematic functions	
	*/
      location_buffer_size += strlen(aux_buffer);
      if(location_buffer_size < LOCATION_BUFFER){
	strcat(location_buffer,aux_buffer);
      	} else {
	  fprintf(stderr,"%s: Not enough room in buffer location_buffer in %s:%d. Aborting\n",ID,__FILE__,__LINE__);
	  errno = ENOBUFS;
	  return errno;
	}
    } else if(!strcmp(aux_buffer,MSGID_TAG)){
      flag = IN_MSGID;

      strcpy(aux_buffer,strstr(buffer,MSGID_TAG)+strlen(MSGID_TAG)+1);
      /*         Overflow WARNING 
       *
       * These strcat are the problematic functions	
       */
      msgid_buffer_size += strlen(aux_buffer);
      if(msgid_buffer_size < MSGID_BUFFER){
	strcat(msgid_buffer,aux_buffer);
      } else {
	fprintf(stderr,"%s: Not enough room in buffer msgid_buffer in %s:%d. Aborting\n",ID,__FILE__,__LINE__);
	errno = ENOBUFS;
	return errno;
      }
    } else if(!strcmp(aux_buffer,MSGSTR_TAG)){
      flag = IN_MSGSTR;

      strcpy(aux_buffer,strstr(buffer,MSGSTR_TAG)+strlen(MSGSTR_TAG)+1);
      /*         Overflow WARNING 
       *
       * These strcat are the problematic functions	
       */
      msgstr_buffer_size += strlen(aux_buffer);
      if(msgstr_buffer_size < MSGSTR_BUFFER){
	strcat(msgstr_buffer,aux_buffer);
      } else {
	fprintf(stderr,"%s: Not enough room in buffer msgstr_buffer in %s:%d. Aborting\n",ID,__FILE__,__LINE__);
	errno = ENOBUFS;
	return errno;
      }
    } else if(aux_buffer[0] == '"'){
      if (flag == IN_MSGID){
	strcpy(aux_buffer,strstr(buffer,"\""));
	/*         Overflow WARNING 
	 *
	 * These strcat are the problematic functions	
	 */
	msgid_buffer_size += strlen(aux_buffer);
	if(msgid_buffer_size < MSGID_BUFFER){
	  strcat(msgid_buffer,aux_buffer);
	} else {
	  fprintf(stderr,"%s: Not enough room in buffer msgid_buffer in %s:%d. Aborting\n",ID,__FILE__,__LINE__);
	  errno = ENOBUFS;
	  return errno;
	}
      }
      if (flag == IN_MSGSTR){
	strcpy(aux_buffer,strstr(buffer,"\""));
	/*         Overflow WARNING 
	 *
	 * These strcat are the problematic functions	
	 */
	msgstr_buffer_size += strlen(aux_buffer);
	if(msgstr_buffer_size < MSGSTR_BUFFER){
	  strcat(msgstr_buffer,aux_buffer);
	} else {
	  fprintf(stderr,"%s: Not enough room in buffer msgstr_buffer in %s:%d. Aborting\n",ID,__FILE__,__LINE__);
	  errno = ENOBUFS;
	  return errno;
	}
      }
    } else if( aux_buffer[0] == '\0'){
      /* We're are in a blank line */
      if(aux_flag == INSIDE_MSG){
	aux_flag = AFTER_MSG;
	flag = IN_WILDERNESS;
      }
    }
  }
  *location = make_safe_for_insertion(location_buffer);
  *comment = make_safe_for_insertion(comment_buffer);
  *msgid = make_safe_for_insertion(msgid_buffer);
  *msgstr = make_safe_for_insertion(msgstr_buffer);

  free(buffer);
  free(aux_buffer);
  free(location_buffer);
  free(comment_buffer);
  free(msgid_buffer);
  free(msgstr_buffer);

  return 0;
}

/*
 * Simply prints the a small help
 */
void usage(){
  printf("Usage: %s [OPTIONS] FILE...\n",ID);
  printf("\n");
  printf(" -H, --host=HOST\t set the hostname or IP of the mysql server\n"); 
  printf(" -u, --user=USER\t set the username used to connect to the mysql server\n");
  printf(" -P, --port=PORT\t set the port where the mysql server is listening\n"); 
  printf(" -D, --database=DB\t set the name of the database to be used\n");
  printf(" -p, --passwd\t\t use password to connect to the mysql server\n");
  printf(" -h, --help\t\t prints this small help\n");
  printf("\n");
}

/*
 * Prints version and copyright
 */
void version(){
  printf("%s %s\n",ID,VERSION);
  printf("Written by Filipe Maia <fmaia@gmx.net>\n");
  printf("\n");
  printf("Copyright (C) 2002 Filipe Maia\n");
  printf("This is free software; see the source for copying conditions.  There is NO\n");
  printf("warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n");
}

/*
 * Connect to the database according to the values of the global variables 
 * and if succesful points the argument to the created MYSQL * database.
 */
int connect(MYSQL * mysql){
  /*
   * I'm using a static variable for passwd instead of a pointer for security
   * This way i don't have to dinamically allocate memory
   */

  char passwd[100];
  int i;
  char c;
  char * pointer_to_passwd;
  char * buffer;

  struct termios initial_set,new_set;

  memset(passwd,0,100);

  if(use_passwd){
    printf("Password: ");
    tcgetattr(0,&initial_set);
    new_set= initial_set;
    new_set.c_lflag &= ~ECHO;
    tcsetattr(0,TCSANOW,&new_set);
    i = 0;
    do{
      c = getchar();
      if (c != '\n')
	passwd[i] = c;
      i++;    
    } while( c !='\n' && i<100);
    tcsetattr(0,TCSANOW,&initial_set);
    printf("\n");
  }
  
  if(!passwd[0]){
    pointer_to_passwd = NULL;
  } else {
    pointer_to_passwd = passwd;
  }

  if (!mysql_real_connect(mysql,host,user,pointer_to_passwd,NULL,port,NULL,0)) {
    fprintf(stderr, "%s: Failed to connect to database: Error: %s\n",ID,
	    mysql_error(mysql));
    return ECONNABORTED;
  } else {

    /* Now we'll try to load the correct database */
    if(!database){
      fprintf(stderr,"%s: Null database in %s:%d. This should NEVER happen!\n",ID,__FILE__,__LINE__);
      return -1;
    }

    buffer = (char *)malloc(sizeof(char)*(strlen(database)+strlen("CREATE DATABASE IF NOT EXISTS ")+2));      
    sprintf(buffer,"CREATE DATABASE IF NOT EXISTS %s;",database);
    if( mysql_query(mysql,buffer) ){
      fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
      return -1;
    }
    free(buffer);      

    if( mysql_select_db(mysql,database) ){
      /* There is no way, at least that i know of, to check if the action was refused
       * by lack of priviledges. So i'm just using the error value */
      fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
      return -1;
    }
    /*    printf("Connected...\n");*/
  }  
  return 0;
}

/*
 * This function is used to clean up all the mess before quiting
 */
int close_connection(MYSQL * mysql){
  mysql_close(mysql); 
  /*  printf("Connection closed.\n");*/
  return 0;
}
  
/*
 * Compares mysql DATETIME values
 *
 * Return value:
 * 
 * Return 1 is datetime1 is greater than datetime2, 0 is they are equal 
 * and 2 if datetime2 is greater than datetime1. In case of error returns -1 and sets errno.
 */
int compare_datetimes( char * datetime1, char * datetime2){
  int year1,year2;
  int month1,month2;
  int day1,day2;
  int hour1,hour2;
  int minute1,minute2;
  int second1,second2;
  if(!datetime1 || !datetime2){
    fprintf(stderr,"%s: Passing null pointer as string in %s:%d\n",ID,__FILE__,__LINE__);
    return -1;
    errno = 22;
  }
  sscanf(datetime1,"%d-%d-%d %d:%d:%d",&year1,&month1,&day1,&hour1,&minute1,&second1);
  sscanf(datetime2,"%d-%d-%d %d:%d:%d",&year2,&month2,&day2,&hour2,&minute2,&second2);
  if(year1 > year2){
    return 1;
  } else if(year2 > year1){
    return 2;
  } else{
    if(month1 > month2){
      return 1;
    } else if(month2 > month1){
      return 2;
    } else {
      if(day1 > day2){
	return 1;
      } else if (day2 > day1){
	return 2;
      }	else {
	if(hour1 > hour2){
	  return 1;
	} else if(hour2 > hour1){
	  return 2;
	} else {
	  if(minute1 > minute2){
	    return 1;
	  } else if (minute2 > minute1) {
	    return 2;
	  } else {
	    if (second1 > second2){
	      return 1;
	    } else if( second2 > second1){
	      return 2;
	    } else {
	      return 0;
	    }
	  }
	}
      }
    }
  }
  fprintf(stderr,"I hopefully won't ever read this from stderr. Error in %s:%d\n",__FILE__,__LINE__);
  return -1;
}

/*
 * Transforms a string to make it mysql safe
 *
 * Return value:
 *
 * Returns the mysql safe for insertion string or NULL if it fails. 
 * If it fails errno is set.
 */

char * make_safe_for_insertion(char * string){
  int i,j;
  int unsafe_chars;
  char * result;
  if (!string){
    errno = 22;
    perror(ID);
    return NULL;
  }
  unsafe_chars = 0;
  for( i = 0;string[i] != '\0';i++){
    if (string[i] == 39 || string[i] == '"' || string[i] == '\\' || string[i] == '\'' ){
      unsafe_chars++;
    }
  }
  result = (char *)malloc(sizeof(char)*(strlen(string)+unsafe_chars+1));
  if(!result){
    perror(ID);
    return NULL;
  }
  for( i = 0, j = 0;string[i] != '\0';i++,j++){
    if (string[i] == 39 || string[i] == '"' || string[i] == '\\' || string[i] == '\''){
      /* We must place a escape character before for correct interpretation */
      result[j] = '\\';
      j++;
    }
    result[j] = string[i];
  }
  /* Close the string */
  result[strlen(string)+unsafe_chars] = '\0';
  return result;
}

char * convert_to_GMT(char * string){
  char * result;
  int year,month,day,hour,minute,hour_zone,minute_zone;
  struct tm c_time;
  long int c_time_in_sec;
  struct tm tzone;
  long int tzone_in_sec;
  struct tm aux;
  long int aux_in_sec;
  char * buffer;
  char * aux_buffer;
  buffer = (char *) malloc (sizeof(char)*20);
  aux_buffer = (char *) malloc (sizeof(char)*20);
  if(!buffer || !aux_buffer){
    perror(ID);
    return NULL;
  }
  result = (char *)malloc(sizeof(char)*strlen("0000-00-00 00:00:00")+1);
  if(!result){
    perror(ID);
    return NULL;
  }

  if(!string ){
    errno = 22;
    perror(ID);
    return NULL;
  }
  sscanf(string,"%d-%d-%d %d:%d%d",&year,&month,&day,&hour,&minute,&hour_zone);
  minute_zone = hour_zone;
  hour_zone = hour_zone/100;
  minute_zone -= hour_zone*100;

  sprintf(buffer,"%.4d-%.2d-%.2d %.2d:%.2d:00",year,month,day,hour,minute);
  strptime(buffer,"%Y-%m-%d %H:%M:%S",&c_time);

  strptime("1970-01-01 12:00:00","%Y-%m-%d %H:%M:%S",&aux);


  strptime("1970-01-01 12:00:00","%Y-%m-%d %H:%M:%S",&tzone);
  tzone.tm_hour += hour_zone;
  tzone.tm_min += minute_zone;

  strftime(buffer,20,"%s",&aux);

  aux_in_sec = strtol(buffer,NULL,10);

  strftime(buffer,20,"%s",&tzone);

  tzone_in_sec = strtol(buffer,NULL,10);
  tzone_in_sec -= aux_in_sec;


  strftime(buffer,20,"%s",&c_time);
  c_time_in_sec = strtol(buffer,NULL,10);
  c_time_in_sec -= tzone_in_sec;


  sprintf(aux_buffer,"%ld",c_time_in_sec);

  strptime(aux_buffer,"%s",&c_time);

  strftime(result,20,"%Y-%m-%d %H:%M:%S",&c_time);

  return result;
}

/*
 * Insert a new row in the original table
 */

int insert_in_original(MYSQL * mysql,char * package_name, int package_id, char * creation, char * msgid, char * location, char * notes){
  char * buffer;
  buffer = (char *)malloc(sizeof(char)*(strlen("INSERT INTO ")+strlen(package_name)+strlen(ORIGINAL_SUF)+\
					strlen("(package_id,string,location,cretion,notes) ")+\
					strlen(" VALUES(")+strlen("-2147483648")+strlen(msgid)+\
					strlen(location)+strlen(creation)+strlen(notes)+strlen(",'','','','');")+1));
  if(!buffer){
    perror(ID);
    return errno;
  }
  sprintf(buffer,"INSERT INTO %s%s(package_id,string,location,creation,notes) VALUES(%d,'%s','%s','%s','%s');",\
	  package_name,ORIGINAL_SUF,package_id,msgid,location,creation,notes);
  if( mysql_query(mysql,buffer) ){
    fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
    fprintf(stderr,"buffer - %s\n",buffer);
    printf("108 - %c\n",buffer[108]);
    printf("109 - %c\n",buffer[109]);
    printf("110 - %c\n",buffer[110]);
    printf("108 - %d\n",buffer[108]);
    printf("109 - %d\n",buffer[109]);
    printf("110 - %d\n",buffer[110]);
    return -1;
  }
  free(buffer);
  return 0;
}

/*
 * Insert a new row in the translation table
 */

int insert_in_translations(MYSQL * mysql,char * package_name, int package_id,int string_id,\
			   char * creation,char * revision, char * msgstr, char * idiom, char * notes){
  char * buffer;
  buffer = (char *)malloc(sizeof(char)*(strlen("INSERT INTO ")+strlen(package_name)+\
					strlen("(package_id,string_id,string,creation,revision,notes,idiom) ")+\
					strlen(" VALUES(")+(strlen("-2147483648")*2)+strlen(msgstr)+\
					strlen(creation)+strlen(revision)+strlen(notes)+strlen(idiom)+\
					strlen(",,'','','','','');")+1));
  if(!buffer){
    perror(ID);
    return errno;
  }
  sprintf(buffer,"INSERT INTO %s(package_id,string_id,string,creation,revision,notes,idiom) VALUES(%d,%d,'%s','%s','%s','%s','%s');",\
	  package_name,package_id,string_id,msgstr,creation,revision,notes,idiom);
  if( mysql_query(mysql,buffer) ){
    fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
    fprintf(stderr,"buffer - %s\n",buffer);
    return -1;
  }
  free(buffer);
  return 0;
}

/*
 * This function updates one row of the original table with the given parameters
 */

int update_original(MYSQL * mysql,char * package_name,char * creation,int string_id){
  char * buffer;
  buffer = (char *)malloc(sizeof(char)*(strlen("UPDATE ")+strlen(package_name)+strlen(ORIGINAL_SUF)+\
					strlen(" SET creation = '")+strlen(creation)+strlen("' WHERE string_id = ")+\
					strlen("-2147123123")+strlen(";")+1));
  if(!buffer){
    perror(ID);
    return errno;
  }
  sprintf(buffer,"UPDATE %s%s SET creation = '%s' WHERE string_id = %d;",package_name,ORIGINAL_SUF,creation,string_id);
  if( mysql_query(mysql,buffer) ){
    fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
    fprintf(stderr,"buffer - %s\n",buffer);
    return -1;
  }
  free(buffer);
  return 0;
}

/*
 * This function updates one row of the translation table with the given parameters
 */

int update_translation(MYSQL * mysql,char * package_name,char * revision, char * msgstr, int string_id){
  char * buffer;
  buffer = (char *)malloc(sizeof(char)*(strlen("UPDATE ")+strlen(package_name)+strlen(" SET revision = '")+\
					strlen(revision)+strlen("',string = '")+strlen(msgstr)+\
					strlen("' WHERE string_id = ")+\
					strlen("-2147483648")+strlen(";")+1));
  if(!buffer){
    perror(ID);
    return errno;
  }
  sprintf(buffer,"UPDATE %s SET string = '%s',revision = '%s' WHERE string_id = %d;",\
	  package_name,msgstr,revision,string_id);
  if( mysql_query(mysql,buffer) ){
    fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
    fprintf(stderr,"buffer - %s\n",buffer);
    return -1;
  }
  free(buffer);	  
  return 0;
}

/*
 * This functions searches for the string_id that matches this msgid, location and notes 
 *
 * Return value:
 *
 * This function returns -1 if there was as error, 0 if it was unable to find a matching string_id
 * or the string_id it's searching for if it founds one.
 */

int search_string_id_in_original(MYSQL * mysql, char * package_name,char * msgid,char * location,char * notes){
  char * buffer;
  MYSQL_RES * res;
  MYSQL_ROW row;
  int result;
  buffer = (char *)malloc(sizeof(char)*(strlen("SELECT string_id FROM ")+strlen(package_name)+strlen(ORIGINAL_SUF)+\
					strlen(" WHERE string = '")+strlen(msgid)+strlen("' AND location = '")+\
					strlen(location)+strlen("' AND notes = '")+strlen(notes)+strlen("';")+1));
  if(!buffer){
    perror(ID);
    return errno;
  }
  sprintf(buffer,"SELECT string_id FROM %s%s WHERE string = '%s' AND location = '%s' and notes = '%s';",package_name,\
	  ORIGINAL_SUF,msgid,location,notes);
  if( mysql_query(mysql,buffer) ){
    fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
    fprintf(stderr,"buffer - %s\n",buffer);
    return -1;
  }
  free(buffer);
  res = mysql_store_result(mysql);
  row = mysql_fetch_row(res);
  if(row){
    result = atoi(row[0]);
  } else {
    result = 0;
  } 
  mysql_free_result(res);
  return result;
}

/*
 * This functions searches for the given string_id in the translated table
 *
 * Return value:
 *
 * This function returns -1 if there was as error, 0 if it was unable to find a row that 
 * matched the given string_id or 1 if it founds a match.
 */


int string_exists_in_translated(MYSQL * mysql,char * package_name,int string_id,char * idiom){
  char * buffer;
  MYSQL_RES * res;
  MYSQL_ROW row;
  int result;
  
  buffer = (char *)malloc(sizeof(char)*(strlen("SELECT string_id FROM ")+strlen(package_name)+\
					strlen(" WHERE string_id =  AND idiom = '';")+strlen("-2123456789")+strlen(idiom)+1));
  if(!buffer){
    perror(ID);
    return errno;
  }
  sprintf(buffer,"SELECT string_id FROM %s WHERE string_id = %d AND idiom = '%s'",package_name,string_id,idiom);
  if( mysql_query(mysql,buffer) ){
    fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
    fprintf(stderr,"buffer - %s\n",buffer);
    return -1;
  }
  free(buffer);
  res = mysql_store_result(mysql);
  row = mysql_fetch_row(res);
  if(row){
    result = 1;
  } else {
    result = 0;
  } 
  mysql_free_result(res);
  return result;
}
  

/*
 * Updates the cretion time in the translated table for the row that match the string_id
 * It checks if the table exists before inserting
 */
int update_creation_in_translations(MYSQL * mysql,char * package_name,char * creation,int string_id){
  char * buffer;
  MYSQL_RES * res;
  MYSQL_ROW row;

  buffer = (char *)malloc(sizeof(char)*(strlen("UPDATE ")+strlen(package_name)+strlen("SET creation = '")+strlen(creation)+\
					strlen("' WHERE string_id = ")+strlen("-2123123123")+strlen(";")+1));
  if(!buffer){
    perror(ID);
    return errno;
  }
  res = mysql_list_tables(mysql,package_name);
  row = mysql_fetch_row(res);
  if(row){
    sprintf(buffer,"UPDATE %s SET creation = '%s' WHERE string_id = %s;",package_name,creation,row[0]);
    if( mysql_query(mysql,buffer) ){
      fprintf(stderr,"%s: Error: %s\n",ID,mysql_error(mysql));
      fprintf(stderr,"buffer - %s\n",buffer);
      return -1;
    }
  }
  mysql_free_result(res);
  free(buffer);
  return 0;
}
